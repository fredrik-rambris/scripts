#!/usr/bin/env python

# Renames files to my own format and moves it to my nas

import os
from os import path
import re
import smbc
import sys

yify_file_regex = re.compile('^(.*)\.(\d{4})\.(1080p|720p)\.(\w+)\.(\w+)\.YIFY\.(mp4|mkv|srt)$')
movie_destination = 'smb://nas/data/Movies'

def auth_fn(server, share, workgroup, username, password):
    return ('WORKGROUP', 'username', 'password')

#tr_app_version = os.environ['TR_APP_VERSION']
#tr_time_localtime = os.environ['TR_TIME_LOCALTIME']
#tr_torrent_dir = os.environ['TR_TORRENT_DIR']
#tr_torrent_hash = os.environ['TR_TORRENT_HASH']
#tr_torrent_id = os.environ['TR_TORRENT_ID']
#tr_torrent_name = os.environ['TR_TORRENT_NAME']
#torrent_dir = path.join(tr_torrent_dir, tr_torrent_name)

torrent_dir = sys.argv[1]

ctx = smbc.Context(auth_fn=auth_fn)
#ctx.timeout=60000

for fname in os.listdir(torrent_dir):
    match = yify_file_regex.match(fname)
    if match:
        new_fname = '%s (%s).%s' % (match.group(1).replace('.', ' ').strip(), match.group(2), match.group(6))
        src_path = path.join(torrent_dir, fname)
        dst_path = movie_destination + "/" + new_fname

        file_size = path.getsize(src_path)
        written_bytes = 0

        print "Copying " + src_path + " to " + dst_path
        src_file = open(src_path, "r")
        dst_file = ctx.open(dst_path, os.O_CREAT | os.O_WRONLY | os.O_TRUNC)

        while True:
            data = src_file.read(1048576)
            if not data:
                break
            try:
                dst_file.write(data)
            except smbc.TimedOutError:
                print "Timeout"

            written_bytes += len(data)
            written_percent = float(written_bytes) / float(file_size) * float(100)

            sys.stdout.write("\r%.0f" % written_percent)
            sys.stdout.flush()
        src_file.close()
        dst_file.close()
        print
