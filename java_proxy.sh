#!/usr/bin/env bash

# Extracts system proxy variables and sets JAVA_PROXY_OPTS if needed

[ -f /etc/environment ] && . /etc/environment

re="://(.+)(:([0-9]+))"

JAVA_PROXY_OPTS=""
if [[ "$http_proxy" =~ $re ]] ; then
    [ -z "${BASH_REMATCH[1]}" ] || JAVA_PROXY_OPTS="$JAVA_PROXY_OPTS -Dhttp.proxyHost=${BASH_REMATCH[1]}"
    [ -z "${BASH_REMATCH[3]}" ] || JAVA_PROXY_OPTS="$JAVA_PROXY_OPTS -Dhttp.proxyPort=${BASH_REMATCH[3]}"
fi

if [[ "$https_proxy" =~ $re ]] ; then
    [ -z "${BASH_REMATCH[1]}" ] || JAVA_PROXY_OPTS="$JAVA_PROXY_OPTS -Dhttps.proxyHost=${BASH_REMATCH[1]}"
    [ -z "${BASH_REMATCH[3]}" ] || JAVA_PROXY_OPTS="$JAVA_PROXY_OPTS -Dhttps.proxyPort=${BASH_REMATCH[3]}"
fi

if ! [ -z "$no_proxy" ] ; then
    IFS=',' read -ra ADDR <<< "$no_proxy"
    tLen=${#ADDR[@]}
    for (( i=0; i<${tLen}; i++ ));
    do
        if [ "${ADDR[$i]:0:1}" == "." ] ; then
            ADDR[$i]="*${ADDR[$i]}"
        fi
    done
    JAVA_PROXY_OPTS="$JAVA_PROXY_OPTS -Dhttp.nonProxyHosts=$(IFS="|";echo "${ADDR[*]}")"
fi

export JAVA_PROXY_OPTS
