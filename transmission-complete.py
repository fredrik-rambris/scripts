#!/usr/bin/env python

from __future__ import absolute_import, print_function, unicode_literals
import sys
import os
import transmissionrpc
from pprint import pprint
from guessit import guessit
from shutil import copyfile
from ConfigParser import ConfigParser
from snack import *
import time
import select

class TransmissionCopy():

    BUFFER_SIZE = 16384

    def __init__(self):
        defaults = {
            'address': 'localhost',
            'port': 9091,
            'username': None,
            'password': None,
            'tvshows_dir': '/tvshows',
            'movies_dir': '/movies'
        }
        self.screen = None
        self.config = ConfigParser(defaults, allow_no_value=True)
        self.config.read(['/etc/transmission-copy.cfg', os.path.expanduser('~/.transmission-copy.cfg')])

        self.tc = transmissionrpc.Client(   address=self.config.get('transmission', 'address'),
                                            port=self.config.getint('transmission', 'port'),
                                            user=self.config.get('transmission', 'user'),
                                            password=self.config.get('transmission', 'password')
                                        )

    def init_gui(self):
        if not self.screen:
            self.screen = SnackScreen()

    def close_gui(self):
        if self.screen:
            self.screen.finish()
            self.screen = None

    def select_torrent(self):
        self.init_gui()
        choices=[]
        for torrent in self.tc.get_torrents():
            if torrent.status != 'downloading' and torrent.percentDone == 1:
                choices.append( (torrent.name, torrent.id) )

        lbcw = ListboxChoiceWindow(self.screen, 'Transmission', 
                    'Select a torrent', 
                    choices)
        self.screen.popWindow()

        if not lbcw[0] == 'cancel':
            return lbcw[1]
        else:
            return None



    def is_esc_pressed(self):
        i,o,e = select.select([sys.stdin],[],[],0.0001)
        for s in i:
            if s == sys.stdin:
                x=sys.stdin.read(1)[0]
                if x == chr(27):
                    return True
                    break
        return False

    def delete(self, torrentid):
        t = self.tc.get_torrent(torrentid)
        if not (t.status == 'stopped' and t.percentDone == 1):
            return
        if self.screen:
            bcw = ButtonChoiceWindow(   self.screen,
                                        'Transmission',
                                        'Do you want to delete this torrent?',
                                        buttons=['Yes', 'No']
                                    )
            self.screen.popWindow()
            if bcw == 'yes':
                self.tc.remove_torrent(torrentid, delete_data=True)


    def copy(self, src, dest):
        if self.screen:
            size = os.path.getsize(src)
            aborted = True
            with open(src, 'rb') as infile, open(dest, 'wb') as outfile:
                pos = 0
                grid = Grid(1,1)
                scale = Scale(40,size/1024)
                scale.set(0)
                grid.setField(scale, 0, 0)
                self.screen.gridWrappedWindow(grid, "Copying file")
                form = Form()
                form.add(grid)
                aborted = False
                while True:
                    piece = infile.read(self.BUFFER_SIZE)
                    if len(piece) == 0:
                        break;
                    if self.is_esc_pressed():
                        aborted = True
                        break;
                    pos += len(piece)

                    scale.set(pos/1024)
                    form.draw()
                    self.screen.refresh()
                    outfile.write(piece)
                return True

            if aborted:
                os.remove(dest)
                return False


        else:
            copyfile(src, dest)
            return True

    def get_paths(self, torrentid):
        torrent = self.tc.get_torrent(torrentid)
        files = sorted(torrent.files().values(), key=lambda k: k['size'], reverse=True) 
        fname = os.path.basename(files[0]['name'])
        guess = guessit(fname)
        if guess['type'] == "movie":
            dest = "%s/%s (%d)%s" % (self.config.get('transmission-complete', 'movies_dir'), guess['title'].title(), guess['year'], os.path.splitext(fname)[1])
        elif guess['type'] == "episode":
            title = guess['title'].title().replace(" Of ", " of ")
            if 'episode_title' in guess:
                fname = "%s.S%02dE%02d.%s.%s" % (title.replace(' ', '.'), guess['season'], guess['episode'], guess['episode_title'], guess['container'] )

            dest = "%s/%s/Season %d/%s" % (self.config.get('transmission-complete', 'tvshows_dir'), guess['title'].title().replace(" Of ", " of "), guess['season'], fname)

        src = os.path.join(torrent.downloadDir, files[0]['name'])

        return (src, dest)

def main():
    try:
        app = TransmissionCopy()
        try:
            torrentid = int(os.environ['TRTR_TORRENT_ID'])
            src, dest = app.get_paths(torrentid)
            app.copy(src, dest)
        except KeyError as e:
            while True:
                torrentid = app.select_torrent()
                if not torrentid: break

                src, dest = app.get_paths(torrentid)
                if app.copy(src, dest):
                    app.delete(torrentid)
    finally:
        if app.screen:
            app.close_gui()
        pass


if __name__ == '__main__':
    sys.exit(main())
